(function (root) {
  var Hanoi = root.Hanoi = (root.Hanoi || {});

  // var readline = require('readline');
  // var READER = readline.createInterface({
  //   input: process.stdin,
  //   output: process.stdout
  // });

  var Game = Hanoi.Game = function () {
    this.towers = [[3, 2, 1], [], []];
};

  Game.prototype.isWon = function () {
    // move all the discs to the last tower
    return (this.towers[2].length == 3) || (this.towers[1].length == 3);
  };

  Game.prototype.isValidMove = function (startTowerIdx, endTowerIdx) {
    var startTower = this.towers[startTowerIdx];
    var endTower = this.towers[endTowerIdx];

    if (startTower.length === 0) {
      return false;
    } else if (endTower.length == 0) {
      return true;
    } else {
      var topStartDisc = startTower[startTower.length - 1];
      var topEndDisc = endTower[endTower.length - 1];
      return topStartDisc < topEndDisc;
    }
  };

  Game.prototype.move = function (startTowerIdx, endTowerIdx) {
    if (this.isValidMove(startTowerIdx, endTowerIdx)) {
      this.towers[endTowerIdx].push(this.towers[startTowerIdx].pop());
      return true;
    } else {
      return false;
    }
  };

  Game.prototype.run = function () {
    var game = this;

    // READER.question("Enter a starting tower: ",function (start) {
 //      var startTowerIdx = parseInt(start);
 //      READER.question("Enter an ending tower: ", function (end) {
 //        var endTowerIdx = parseInt(end);
 //        game.takeTurn(startTowerIdx,endTowerIdx);
 //      });
 //    });
  };

  Game.prototype.takeTurn = function (start,end){
    var game = this;

    if (game.move(start,end)) {
      alert(game.towers);
    } else {
      alert("Invalid move!")
    }

    if (game.isWon()) {
      alert("You win!");
      //READER.close();
    } else {
      game.run();
    }
  }

  var TowersUI = Hanoi.TowersUI = function() {
    this.game = new Hanoi.Game();
    this.towers = this.game.towers;
  };

  TowersUI.prototype.render = function() {
    var page = $('body');
    var that = this;
    page.html('THAT GAME');
    //alert(this.towers);
    this.towers.forEach( function(pile, rowIndex) {

      page.append("<div class='row' id=" + rowIndex + ">row " + rowIndex + "</div>");

      for (var i = 0; i < 3; i++) {
        var jDisc = $("<div class='cell'></div>");
        var disc = pile[i];
        jDisc.addClass(that.discify(disc));
        page.append(jDisc);
      }
    });
    that.installSourceClickHandler();
  }

  TowersUI.prototype.discify = function(disc) {
    switch(disc) {
    case 1:
      return "small";
    case 2:
      return "medium";
    case 3:
      return "large";
    default:
      return "";
    }
  }

  TowersUI.prototype.installSourceClickHandler = function() {
    var that = this;
    $('.row').off();
    $('.row').on('click', function() {
      that.source = parseInt(this.id);
      $(this).addClass('highlight');
      that.installDestClickHandler();
    })
  }

  TowersUI.prototype.installDestClickHandler = function() {
    var that = this;
    $('.row').off();
    $('.row').on('click', function() {
      that.dest = parseInt(this.id);
      that.game.takeTurn(that.source, that.dest);
      that.playTurn();
    });
  }

  TowersUI.prototype.playTurn = function() {
    this.render();
    var that = this;
    alert( typeof that.source );
  }


})(this);

// this.Hanoi.Game is a constructor function, so we instantiate a new object, then run it.

var Game = new this.Hanoi.Game();
Game.run();

var Towers = new this.Hanoi.TowersUI();
Towers.playTurn();
